# OSE Magento PHPCS Ruleset

## Description

This ruleset is meant to be used on Magento projects and modules. It combines [PSR-12](https://www.php-fig.org/psr/psr-12/), [Magento Coding Standards](https://github.com/magento/magento-coding-standard) and custom rules from [Slevomat Coding Standard](https://github.com/slevomat/coding-standard)

## Installation

To use this ruleset, require it in composer:

```shell
composer require --dev ose/magento2-phpcs
```

## Usage

You can check this custom phpcs ruleset has been installed with this command:

```shell
php vendor/bin/phpcs -i
```

You can run phpcs on the `app/code/Ose` folder with this command:

```shell
php vendor/bin/phpcs --standard=magento2-phpcs app/code/Ose
```